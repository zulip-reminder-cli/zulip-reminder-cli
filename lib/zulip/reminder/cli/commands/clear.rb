# frozen_string_literal: true

require 'zulip/reminder/config'
require_relative '../command'

module Zulip
  module Reminder
    module Cli
      module Commands
        class Clear < Zulip::Reminder::Cli::Command
          def initialize(options)
            @options = options
            @config = Zulip::Reminder::Config.new
          end

          def execute(input: $stdin, output: $stdout)
            path = @config.task_path
            @data = {}
            open(@config.task_path, "w+") do |file|
              file.puts(YAML.dump(@data))
            end
          end
        end
      end
    end
  end
end
