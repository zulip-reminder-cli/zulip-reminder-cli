# frozen_string_literal: true

require 'zulip/reminder/config'
require 'zulip/client'
require_relative '../command'

module Zulip
  module Reminder
    module Cli
      module Commands
        class Delete < Zulip::Reminder::Cli::Command
          def initialize(type, message, options)
            @type = type
            @message = message
            @options = options
            @config = Zulip::Reminder::Config.new
          end

          def execute(input: $stdin, output: $stdout)
            path = @config.task_path
            unless path.exist?
              puts "No registered jobs"
              return
            end

            @data = YAML.load_file(path.to_s)
            if @data.empty?
              puts "No registered jobs"
              return
            end

            job_id = detect_job_id
            if job_id.nil?
              puts "No registered jobs"
              return
            end

            content = "#{@config.bot_name} delete job #{job_id}"
            puts "Delete: #{content}"
            client = Zulip::Client.new(site: @config.site,
                                       username: @config.user_email,
                                       api_key: @config.key)
            if @config.debug?
              client.send_private_message(to: @config.user_email, content: content)
            else
              client.send_public_message(to: @config.stream, subject: @config.topic, content: content)
            end
          end

          private
          def detect_job_id
            job_id = @data.keys.first
            case @type
            when "last"
              job_id = @data.keys.last
            else
              job_id = @data.keys.first
              if @type =~ /(\d+)/
                job_id = @type
              end
            end
            job_id
          end
        end
      end
    end
  end
end
