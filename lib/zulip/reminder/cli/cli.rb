# frozen_string_literal: true

require 'thor'

module Zulip
  module Reminder
    module Cli
      # Handle the application command line parsing
      # and the dispatch to various command objects
      #
      # @api public
      class CLI < Thor
        # Error raised by this runner
        Error = Class.new(StandardError)

        desc 'version', 'zulip-reminder-cli version'
        def version
          require_relative 'version'
          puts "v#{Zulip::Reminder::Cli::VERSION}"
        end
        map %w(--version -v) => :version

        desc 'clear', 'Clear task history'
        method_option :help, aliases: '-h', type: :boolean,
                             desc: 'Display usage information'
        def clear(*)
          if options[:help]
            invoke :help, ['clear']
          else
            require_relative 'commands/clear'
            Zulip::Reminder::Cli::Commands::Clear.new(options).execute
          end
        end

        desc 'list', 'Command description...'
        method_option :help, aliases: '-h', type: :boolean,
                             desc: 'Display usage information'
        def list(*)
          if options[:help]
            invoke :help, ['list']
          else
            require_relative 'commands/list'
            Zulip::Reminder::Cli::Commands::List.new(options).execute
          end
        end

        desc 'delete TYPE_OR_JOB_ID [MESSAGE]', 'Command description...'
        method_option :help, aliases: '-h', type: :boolean,
                             desc: 'Display usage information'
        def delete(type, message="")
          if options[:help]
            invoke :help, ['delete']
          else
            require_relative 'commands/delete'
            Zulip::Reminder::Cli::Commands::Delete.new(type, message, options).execute
          end
        end

        desc 'add TIMESTAMP MESSAGE [-s stream] [-t topic]', 'Add a reminder to Zulip bot'
        method_option :help, aliases: '-h', type: :boolean,
                             desc: 'Display usage information'
        method_option :stream, aliases: '-s', type: :string,
                             desc: 'Specify target stream'
        method_option :topic, aliases: '-t', type: :string,
                             desc: 'Specify target topic'
        def add(timestamp, message)
          if options[:help]
            invoke :help, ['add']
          else
            require_relative 'commands/add'
            Zulip::Reminder::Cli::Commands::Add.new(timestamp, message, options).execute
          end
        end
      end
    end
  end
end
