require "zulip/reminder/cli/version"

module Zulip
  module Reminder
    module Cli
      class Error < StandardError; end
      class EmptyTaskError < StandardError; end
    end
  end
end
